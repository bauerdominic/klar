package convert

import (
	"fmt"
	"strings"

	log "github.com/sirupsen/logrus"

	"gitlab.com/gitlab-org/security-products/analyzers/common/v2/issue"
	"gitlab.com/gitlab-org/security-products/analyzers/klar/v2/metadata"
	"gitlab.com/gitlab-org/security-products/analyzers/klar/v2/urls"
)

func (r result) toIssue() *issue.Issue {
	// convert the KlarVulnerability into a Security Products Common Format issue
	return &issue.Issue{
		Category:    issue.CategoryContainerScanning,
		Scanner:     metadata.IssueScanner,
		Message:     r.message(),
		Description: r.issueDescription(),
		Solution:    r.solution(),
		Severity:    r.level(),
		Links:       issue.NewLinks(r.Link),
		Location:    r.location(),
		CompareKey:  r.compareKey(),
		Identifiers: r.identifiers(),
		Confidence:  issue.ConfidenceLevelUnknown,
	}
}

func (r result) compareKey() string {
	return strings.Join([]string{r.NamespaceName, r.FeatureName, r.Name}, ":")
}

func (r result) location() issue.Location {
	return issue.Location{
		OperatingSystem: r.NamespaceName,
		Image:           r.dockerImageName,
		Dependency: &issue.Dependency{
			Package: issue.Package{r.FeatureName},
			Version: r.FeatureVersion,
		},
	}
}

func (r result) message() string {
	if r.Name != "" && r.FeatureName != "" {
		return fmt.Sprintf("%s in %s", r.Name, r.FeatureName)
	}

	return r.Name
}

func (r result) issueDescription() string {
	if r.Description != "" {
		return r.Description
	}

	if r.FeatureName != "" && r.FeatureVersion != "" {
		return fmt.Sprintf("%s:%s is affected by %s", r.FeatureName, r.FeatureVersion, r.Name)
	}

	if r.FeatureName != "" {
		return fmt.Sprintf("%s is affected by %s", r.FeatureName, r.Name)
	}

	if r.NamespaceName != "" {
		return fmt.Sprintf("%s is affected by %s", r.NamespaceName, r.Name)
	}

	return ""
}

func (r result) solution() string {
	if r.FixedBy != "" && r.FeatureName != "" && r.FeatureVersion != "" {
		return fmt.Sprintf("Upgrade %s from %s to %s", r.FeatureName, r.FeatureVersion, r.FixedBy)
	}

	if r.FixedBy != "" && r.FeatureName != "" {
		return fmt.Sprintf("Upgrade %s to %s", r.FeatureName, r.FixedBy)
	}

	if r.FixedBy != "" {
		return fmt.Sprintf("Upgrade %s", r.FixedBy)
	}

	return ""
}

// level returns the normalized severity.
func (r result) level() issue.SeverityLevel {
	switch r.Severity {
	case "Critical", "Defcon1":
		return issue.SeverityLevelCritical
	case "High":
		return issue.SeverityLevelHigh
	case "Medium":
		return issue.SeverityLevelMedium
	case "Negligible", "Low":
		return issue.SeverityLevelLow
	}

	return issue.SeverityLevelUnknown
}

// identifiers returns the normalized identifiers of the vulnerability.
func (r result) identifiers() []issue.Identifier {
	if identifier, ok := issue.ParseIdentifierID(r.Name); ok {
		return []issue.Identifier{identifier}
	}

	log.Warnf("Unknown vulnerability ID '%s', allowlist support disabled for vulnerability.  Please report this by submitting an issue to GitLab by visiting %s.", r.Name, urls.NewIssue)
	return []issue.Identifier{}
}
