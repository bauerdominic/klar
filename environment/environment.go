package environment

import (
	"errors"
	"fmt"
	"os"
	"strconv"

	log "github.com/sirupsen/logrus"

	"gitlab.com/gitlab-org/security-products/analyzers/klar/v2/urls"
)

var (
	// errDockerImageNotProvided is used when a docker image value hasn't been provided
	errDockerImageNotProvided = errors.New("a Docker image value must be provided")

	// errApplicationTagOrCommitShaNotProvided is used to describe an error related to missing environment variables
	errApplicationTagOrCommitShaNotProvided = errors.New("a Docker image value must be provided. It looks like you've provided a CI_APPLICATION_REPOSITORY environment variable, but haven't provided a CI_APPLICATION_TAG or CI_COMMIT_SHA environment variable")

	// errCommitShaNotProvided is used to describe an error related to missing environment variables
	errCommitShaNotProvided = errors.New("a Docker image value must be provided. It looks like you've provided both CI_REGISTRY_IMAGE and CI_COMMIT_REF_SLUG environment variables, but haven't provided a CI_COMMIT_SHA environment variable")

	// If the number of vulnerabilities detected is greater than the CLAIR_THRESHOLD
	// value, then klar will return with an exit code of `1`, otherwise an exit code
	// of `0` will be returned. If an actual error is encountered, then klar will
	// return with an exit code of `2`.
	envVarVulnerabilityThreshold = "CLAIR_THRESHOLD"

	// Output format of the vulnerabilities. Supported formats are standard, json, table.
	envVarKlarOutputFormat = "FORMAT_OUTPUT"

	// Address of Clair server. It has a form of protocol://host:port - protocol and port default
	// to http and 6060 respectively and may be omitted. You can also specify basic authentication
	// in the URL: protocol://login:password@host:port.
	envVarClairAddress = "CLAIR_ADDR"

	// The environment variable klar uses for pointing to the YAML file with the CVE allowlist.
	// This variable must be unset so as not to interfere with our own allowlisting implementation
	envVarWhitelistFile = "WHITELIST_FILE"

	// Severity level threshold, vulnerabilities with severity level higher than or equal to
	// this threshold will be outputted. Supported levels are Unknown, Negligible, Low, Medium,
	// High, Critical, Defcon1.
	// TODO: Deprecate `CLAIR_OUTPUT` and replace with `SEVERITY_LEVEL_THRESHOLD`
	// See https://gitlab.com/gitlab-org/gitlab/issues/36195 for details
	envVarSeverityLevelThreshold = "CLAIR_OUTPUT"

	// EnvVarKlarTrace enables more verbose output from klar when set to true.
	EnvVarKlarTrace = "KLAR_TRACE"

	// envVarClairTrace enables more verbose output from clair when set to true.
	envVarClairTrace = "CLAIR_TRACE"

	// EnvVarRegistryInsecure allows Klar to access insecure registries (HTTP only).
	// Should only be set to true when testing the image locally.
	EnvVarRegistryInsecure = "REGISTRY_INSECURE"

	// EnvVarDockerInsecure allows Klar to access registries with bad (or self-signed) SSL
	// certificates. Default is false. Clair will need to be booted with -insecure-tls for
	// this to work
	EnvVarDockerInsecure = "DOCKER_INSECURE"

	// EnvVarDockerImage is used for providing the Docker image to be scanned
	EnvVarDockerImage = "DOCKER_IMAGE"

	// EnvVarBypassVulnerabilitiesDBServerCheck is used for tests to bypass checking the
	// vulnerabilities database to ensure it's running, otherwise our tests would require
	// postgres to be running
	EnvVarBypassVulnerabilitiesDBServerCheck = "BYPASS_VULNERABILITIES_DB_CHECK"

	// Environment variables which shouldn't be changed.
	unconfigurableEnvVarValues = map[string]string{
		// envVarVulnerabilityThreshold is set to 0 by default according to the klar docs,
		// but we set it here explicitly in case it ever changes in the future, which would
		// break our code
		envVarVulnerabilityThreshold: "0",
		envVarKlarOutputFormat:       "json",
		envVarClairAddress:           "localhost",
	}

	// Environment variables which can be configured by the user
	configurableEnvVarValues = map[string]string{
		envVarSeverityLevelThreshold: "Unknown",
		EnvVarKlarTrace:              "false",
		EnvVarRegistryInsecure:       "false",
		EnvVarDockerInsecure:         "false",
		envVarClairTrace:             "false",
	}
)

// DockerImageName constructs the name of the docker image to be scanned, based
// on various environment variables, mostly for providing backwards compatibility
// with previous implementations of the container scanning tool
func DockerImageName() (string, error) {
	// first try the DOCKER_IMAGE environment variable
	dockerImageEnvVar := os.Getenv(EnvVarDockerImage)

	if dockerImageEnvVar != "" {
		return dockerImageEnvVar, nil
	}

	// next try CI_APPLICATION_REPOSITORY:CI_APPLICATION_TAG
	ciApplicationRepository := os.Getenv("CI_APPLICATION_REPOSITORY")

	if ciApplicationRepository != "" {
		// CI_APPLICATION_REPOSITORY was present
		ciApplicationTag := os.Getenv("CI_APPLICATION_TAG")

		// both CI_APPLICATION_REPOSITORY and CI_APPLICATION_TAG were present, concatenate
		// and use as the docker image name
		if ciApplicationTag != "" {
			return fmt.Sprintf("%s:%s", ciApplicationRepository, ciApplicationTag), nil
		}

		// only CI_APPLICATION_REPOSITORY was present, attempt to use the CI_COMMIT_SHA
		// value as the Docker tag.
		ciCommitSha := os.Getenv("CI_COMMIT_SHA")

		if ciCommitSha == "" {
			return "", errApplicationTagOrCommitShaNotProvided
		}

		return fmt.Sprintf("%s:%s", ciApplicationRepository, ciCommitSha), nil
	}

	// CI_APPLICATION_REPOSITORY was empty, use CI_REGISTRY_IMAGE/CI_COMMIT_REF_SLUG
	ciRegistryImage := os.Getenv("CI_REGISTRY_IMAGE")
	ciCommitRefSlug := os.Getenv("CI_COMMIT_REF_SLUG")

	if ciRegistryImage == "" || ciCommitRefSlug == "" {
		return "", errDockerImageNotProvided
	}

	ciCommitSha := os.Getenv("CI_COMMIT_SHA")

	if ciCommitSha == "" {
		return "", errCommitShaNotProvided
	}

	return fmt.Sprintf("%s/%s:%s", ciRegistryImage, ciCommitRefSlug, ciCommitSha), nil
}

// SetEnvironmentVariablesForKlar configures the environment variables
// necessary for klar to operate.
func SetEnvironmentVariablesForKlar() {
	// since we implement allowlist parsing ourslves in order to have maximum
	// flexibility, we must disable klar allowlisting by explicitly unsetting
	// the allowlist env var in case a user has provided it by mistake
	os.Unsetenv(envVarWhitelistFile)

	warnDeprecatedEnvVars()
	setUnconfigurableEnvVarValues()
	setConfigurableEnvVarValues()
	setDockerCredentials()
}

// DockerCredentialsOverridden will determine if the DOCKER_USER or DOCKER_PASSWORD environment
// variables have been set.  By using `LookupEnv`, it allows us to distinguish unset from empty value
func DockerCredentialsOverridden() bool {
	_, dockerUserIsSet := os.LookupEnv("DOCKER_USER")
	_, dockerPasswordIsSet := os.LookupEnv("DOCKER_PASSWORD")

	if dockerUserIsSet && dockerPasswordIsSet {
		return true
	}

	return false
}

// SeverityLevelThreshold returns the value of the environment variable CLAIR_OUTPUT
func SeverityLevelThreshold() string {
	return os.Getenv(envVarSeverityLevelThreshold)
}

// KlarExecutableVersion is only used for providing output to the user to explain which
// version of klar is being used for the container scan
func KlarExecutableVersion() string {
	return os.Getenv("KLAR_EXECUTABLE_VERSION")
}

// KlarTrace returns the boolean value of the environment variable used for more verbose output from
// the klar process. When set to true, more verbose output will be provided during a container scan.
func KlarTrace() bool {
	klarTrace, _ := strconv.ParseBool(os.Getenv(EnvVarKlarTrace))

	return klarTrace
}

// ClairTrace returns the boolean value of the environment variable used for more verbose output from
// the clair process. When set to true, more verbose output will be provided during a container scan.
func ClairTrace() bool {
	clairTrace, _ := strconv.ParseBool(os.Getenv(envVarClairTrace))

	return clairTrace
}

// RegistryInsecure returns the boolean value of the environment variable used to control whether
// HTTP or HTTPS is used for communicating with the docker registry
// When set to true, klar will communicate with the registry using HTTP, otherwise HTTPS will
// be used
func RegistryInsecure() bool {
	registryInsecure, _ := strconv.ParseBool(os.Getenv(EnvVarRegistryInsecure))

	return registryInsecure
}

// DockerInsecure returns the boolean value of the environment variable used to control whether
// to verify the hostname or certificate chain for the SSL certificate of the Docker registry
func DockerInsecure() bool {
	dockerInsecure, _ := strconv.ParseBool(os.Getenv(EnvVarDockerInsecure))

	return dockerInsecure
}

// KubernetesDetected returns true if the analyzer is executing within a Kubernetes context
func KubernetesDetected() bool {
	if os.Getenv("KUBERNETES_PORT") == "" {
		return false
	}

	return true
}

// BypassVulnerabilitiesDBServerCheck returns the boolean value of the environment variable
// used to control whether we check whether the postgres server is running before starting
// the clair server.
func BypassVulnerabilitiesDBServerCheck() bool {
	bypassVulnerabilitiesDBServerCheck, _ := strconv.ParseBool(os.Getenv(EnvVarBypassVulnerabilitiesDBServerCheck))

	return bypassVulnerabilitiesDBServerCheck
}

func warnDeprecatedEnvVars() {
	// output deprecation warning if CLAIR_DB_IMAGE_TAG is being used
	clairDBImageTag := os.Getenv("CLAIR_DB_IMAGE_TAG")

	if clairDBImageTag != "" && clairDBImageTag != "latest" {
		log.Warnf("DEPRECATION NOTICE: Detected deprecated CLAIR_DB_IMAGE_TAG environment variable.  Please remove this variable and replace with CLAIR_DB_IMAGE. See %s for more details.", urls.AvailableVariables)
	}
}

func setUnconfigurableEnvVarValues() {
	// set the given environment variables to default values
	for envVarName, envVarDefaultValue := range unconfigurableEnvVarValues {
		os.Setenv(envVarName, envVarDefaultValue)
	}
}

func setConfigurableEnvVarValues() {
	for envVarName, envVarDefaultValue := range configurableEnvVarValues {
		// if the user has overridden the given environment variable and provided
		// a custom value, we'll use that
		if os.Getenv(envVarName) != "" {
			log.Infof("Found custom configuration for '%s', setting value to '%s'", envVarName, os.Getenv(envVarName))
			continue
		}

		// user has not provided a custom environment varible value, use the default
		os.Setenv(envVarName, envVarDefaultValue)
	}
}

func setDockerCredentials() {
	// if both the DOCKER_USER or DOCKER_PASSWORD have been configured (even possibly set to a blank value),
	// then use those as the credentials for logging into the docker registry
	if DockerCredentialsOverridden() {
		log.Info("DOCKER_USER and DOCKER_PASSWORD environment variables have been configured, logging into remote docker registry using those credentials.")
		return
	}

	// if either the DOCKER_USER or DOCKER_PASSWORD are not set, then default the
	// DOCKER_USER and DOCKER_PASSWORD to the CI user/password so we can access a private
	// GitLab registry without requiring the user to modify any settings
	log.Info("DOCKER_USER and DOCKER_PASSWORD environment variables have not been configured. Defaulting to DOCKER_USER=$CI_REGISTRY_USER and DOCKER_PASSWORD=$CI_REGISTRY_PASSWORD")
	os.Setenv("DOCKER_USER", os.Getenv("CI_REGISTRY_USER"))
	os.Setenv("DOCKER_PASSWORD", os.Getenv("CI_REGISTRY_PASSWORD"))
}
