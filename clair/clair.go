package clair

import (
	"bytes"
	"database/sql"
	"fmt"
	"io"
	"io/ioutil"
	"os/exec"
	"strings"
	"time"

	// use the postgres database driver
	_ "github.com/lib/pq"
	"github.com/pkg/errors"
	log "github.com/sirupsen/logrus"

	"gitlab.com/gitlab-org/security-products/analyzers/klar/v2/environment"
	"gitlab.com/gitlab-org/security-products/analyzers/klar/v2/urls"
)

var (
	// clairRetryWait is the amount to wait before checking to see if the Clair server
	// is running
	clairRetryWait = time.Duration(2 * time.Second)

	// vulnerabilitiesDBRetryWait is the amount to wait before checking to see if the
	// vulnerabilities database is running
	vulnerabilitiesDBRetryWait = time.Duration(2 * time.Second)

	// maxVulnerabilitiesDBRetryCount is the maximum number of retries to check to see if
	// vulnerabilities database is running before we give up
	maxVulnerabilitiesDBRetryCount = 10

	// maxClairServerRetryCount is the maximum number of retries to check to see if the Clair
	// server is running before we give up
	maxClairServerRetryCount = 10

	// pathToClairBinary is the path to the clair binary file
	pathToClairBinary = "/clair"

	// pathToConfigFileTemplate is the path to the unmodified clair config template file
	pathToConfigFileTemplate = "/clair-config.yaml.template"

	// pathToConfigFile is the path where the modified clair config template file will
	// be saved
	pathToConfigFile = "/clair-config.yaml"

	clairServerCmd *exec.Cmd = nil

	kubernetesDBHostAddr = "127.0.0.1"
)

// StartBackgroundServer starts the clair server in the background
func StartBackgroundServer(startClairServer bool, clairVulnerabilitiesDBConnectionString string) error {
	// The clair server doesn't currently work on OS X since it requires the
	// Redhat Package Management utilities (RPM), so it's useful to disable the
	// clair server when developing on an OS X system, and instead run the clair
	// server process as a separate Docker container
	if !startClairServer {
		log.Warnf("Not starting Clair server process.")
		return nil
	}

	if err := writeConfigFile(clairVulnerabilitiesDBConnectionString); err != nil {
		return err
	}

	// bypass the vulnerabilities db server check because we don't want to require
	// postgres when running tests
	if environment.BypassVulnerabilitiesDBServerCheck() {
		log.Warn("Bypassing Vulnerabilities DB Check")
	} else {
		if err := ensureVulnerabilitiesDBServerReady(clairVulnerabilitiesDBConnectionString); err != nil {
			return err
		}
	}

	// configure exec command to run the clair server with the given commandline args
	clairServerCmd = exec.Command(pathToClairBinary, clairServerArgs()...)

	// store stderr from the clair command so that we can print the clair log contents
	// if the clair process doesn't start immediately
	stderrPipe, err := clairServerCmd.StderrPipe()
	if err != nil {
		return err
	}

	// we need to continuously monitor stdout to determine when the clair server is ready
	// to accept incoming connections
	stdoutPipe, err := clairServerCmd.StdoutPipe()
	if err != nil {
		return err
	}

	// start the clair server
	err = clairServerCmd.Start()
	if err != nil {
		return errors.Wrap(err, "An error occurred while starting the clair server process")
	}

	log.Infof("Started Clair server process with PID: %d", clairServerCmd.Process.Pid)
	log.Infof("Waiting for Clair API to start...")

	stdoutErrCh := make(chan error)
	defer close(stdoutErrCh)
	stopReadStderr := make(chan bool)
	defer close(stopReadStderr)
	stopReadStdout := make(chan bool)
	defer close(stopReadStdout)

	// read from Stdout/Stderr in a goroutine so we can output the clair server details
	// without blocking
	go readStdout(stdoutPipe, stdoutErrCh, stopReadStdout)
	go readStderr(stderrPipe, stopReadStderr)

	retryCount := 1
	// read from stdout until we see "starting main API" output from clair server
	for ; retryCount <= maxClairServerRetryCount; retryCount++ {
		select {
		case err := <-stdoutErrCh:
			// an error occurred while starting the server
			if err != nil {
				return err
			}
			log.Infof("Clair API started successfully.")
			return nil
		default:
			// clair server hasn't yet started, loop and check again
		}

		log.Warnf("Clair API not ready, waiting %s before retrying. Retry %d of %d",
			clairRetryWait, retryCount, maxClairServerRetryCount)

		time.Sleep(clairRetryWait)
	}

	return fmt.Errorf(
		"error while waiting for Clair API to start. Giving up after %d retries",
		retryCount-1)
}

// ShutdownServer kills the clair server process
func ShutdownServer() {
	if clairServerCmd == nil || clairServerCmd.Process == nil {
		return
	}

	// kill the clair server process
	log.Infof("Shutting down Clair server with PID: %d", clairServerCmd.Process.Pid)
	if serverErr := clairServerCmd.Process.Kill(); serverErr != nil {
		log.Fatalf("Failed to shutdown Clair server. Err: %s", serverErr)
		return
	}
	log.Info("Clair server shut down successfully")
}

// DBConnectionString returns an SQL Connection URI (see Section `31.1.1.2. Connection URIs`:
// https://www.postgresql.org/docs/9.3/libpq-connect.html) for connecting to the vulnerabilities DB.
//
// It takes the following parameters:
//
// `clairVulnerabilitiesDBHost` [DEPRECATED]: the hostname parameter for connecting to the vulnerabilities DB.
// TODO: remove this variable. See https://gitlab.com/gitlab-org/gitlab/issues/202242
//
// `clairVulnerabilitiesDBConnectionString`: a complete SQL connection string containing the dbname, host,
// password, port, sslmode and statement_timeout values
//
// Attempt to generate a connection string, checking parameters using the following order:
//
// 1. If a custom `clairVulnerabilitiesDBHost` value has been provided, insert that value into the
//    connection string and return that
// 2. If a custom `clairVulnerabilitiesDBConnectionString` has been provided, return that
// 3. If Kubernetes has been detected, insert a custom kubernetes ip address into the connection
//    string and return that
// 4. Fall back to the default db connection string value and return that
//
// Returns a connection string with the form: "postgresql://<user>:<password>@<vulerabilities-db-host>:<port>/<databse>?sslmode=<enable|disable>&statement_timeout=<timeout>"
func DBConnectionString(clairVulnerabilitiesDBHost, clairVulnerabilitiesDBConnectionString, defaultDBHost, defaultDBConnectionString string) string {
	// determine if a custom host has been provided by the clairVulnerabilitiesDBHost parameter
	if clairVulnerabilitiesDBHost != "" && clairVulnerabilitiesDBHost != defaultDBHost {
		// if a custom host has been provided, replace the host from the
		// clairVulnerabilitiesDBConnectionString value, thereby giving precedence to the
		// clairVulnerabilitiesDBHost parameter
		log.Warnf("DEPRECATION NOTICE: Detected deprecated CLAIR_VULNERABILITIES_DB_URL environment variable. Please remove this variable and replace with CLAIR_DB_CONNECTION_STRING. See %s for more details.",
			urls.AvailableVariables)
		return strings.ReplaceAll(defaultDBConnectionString, defaultDBHost, clairVulnerabilitiesDBHost)
	}

	if clairVulnerabilitiesDBConnectionString != "" && clairVulnerabilitiesDBConnectionString != defaultDBConnectionString {
		// a custom clairVulnerabilitiesDBConnectionString has been provided, use that
		return clairVulnerabilitiesDBConnectionString
	}

	if environment.KubernetesDetected() {
		log.Infof("Detected KUBERNETES_PORT environment variable, using %s for clair vulnerabilities DB connection string", kubernetesDBHostAddr)

		return strings.ReplaceAll(defaultDBConnectionString, defaultDBHost, kubernetesDBHostAddr)
	}

	// kubernetes is not running and no custom db host/connection string values were given,
	// fall back to using the default db connection string
	return defaultDBConnectionString
}

// clairServerArgs contains a list of arguments for starting the clair server.
// The only mandatory argument that's currently provided is the path to the config file
func clairServerArgs() []string {
	serverArgs := []string{fmt.Sprintf("-config=%s", pathToConfigFile)}

	if environment.DockerInsecure() {
		return append(serverArgs, "-insecure-tls")
	}

	return serverArgs
}

// writeConfigFile overrides the Postgres vulnerabilities database connection string used by clair.
func writeConfigFile(clairVulnerabilitiesDBConnectionString string) error {
	clairConfigTemplate, err := ioutil.ReadFile(pathToConfigFileTemplate)
	if err != nil {
		return err
	}

	modifiedConfig := bytes.Replace(clairConfigTemplate,
		[]byte("POSTGRES-VULNERABILITIES-DB-CONNECTION-STRING"), []byte(clairVulnerabilitiesDBConnectionString), -1)

	return ioutil.WriteFile(pathToConfigFile, modifiedConfig, 0644)
}

// When the clair server first starts up, it attempts to open a db connection to the vulnerabilities
// database. If the database is not yet ready, clair returns an error and doesn't retry.  In order
// to workaround this issue, we need to check the database connection before starting clair, and retry
// if it's not yet ready, waiting a few seconds between retries.
func ensureVulnerabilitiesDBServerReady(clairVulnerabilitiesDBConnectionString string) error {
	db, err := sql.Open("postgres", clairVulnerabilitiesDBConnectionString)
	if err != nil {
		// if we can't open a connection to the database, it's a fatal error which should halt execution
		return errors.Wrap(err, "An error occurred while attempting to open a connection to the vulnerabilities database")
	}
	defer db.Close()

	retryCount := 1
	for ; retryCount <= maxVulnerabilitiesDBRetryCount; retryCount++ {
		// if the db isn't ready yet, we can keep retrying until it's running
		err = db.Ping()
		if err == nil {
			log.Infof("Successfully connected to the vulnerabilities database")
			return nil
		}

		log.Warnf("Vulnerabilities database not ready, waiting %s before retrying. Retry %d of %d",
			vulnerabilitiesDBRetryWait, retryCount, maxVulnerabilitiesDBRetryCount)

		time.Sleep(vulnerabilitiesDBRetryWait)
	}

	return errors.Wrapf(err,
		"error while waiting for vulnerabilities database to start. Giving up after %d retries.",
		retryCount-1)
}

// readStdout logs the stdout pipe. It pushes read errors to the error channel,
// and pushes a nil error when the server starts successfully, based on its log.
func readStdout(stdoutPipe io.ReadCloser, errCh chan error, done chan bool) {
	stdoutBuf := make([]byte, 1024, 1024)

	for {
		stdoutNumBytesRead, err := stdoutPipe.Read(stdoutBuf)
		if err != nil {
			// Read returns io.EOF at the end of file, which is not an error for us
			if err != io.EOF {
				errCh <- err
				return
			}
		}

		select {
		case <-done:
			// stop printing log messages unless ClairTrace has been enabled
			if !environment.ClairTrace() {
				return
			}
		default:
		}

		if stdoutNumBytesRead > 0 {
			log.Warnf("Clair log contents: \n  %s", strings.Replace(string(stdoutBuf[0:stdoutNumBytesRead]),
				"\n", "\n  ", -1))
		}

		if stdoutNumBytesRead == 0 {
			continue
		}

		// check to see if the clair server has started
		if strings.Contains(string(stdoutBuf), "starting main API") {
			errCh <- nil
			// stop printing log messages unless ClairTrace has been enabled
			if !environment.ClairTrace() {
				return
			}
		}
	}
}

func readStderr(stderrPipe io.ReadCloser, done chan bool) {
	stderrBuf := make([]byte, 1024, 1024)

	for {
		stderrNumBytesRead, _ := stderrPipe.Read(stderrBuf)

		select {
		case <-done:
			return
		default:
		}

		if stderrNumBytesRead > 0 {
			log.Errorf("Clair error log contents: \n  %s", strings.Replace(string(stderrBuf[0:stderrNumBytesRead]),
				"\n", "\n  ", -1))
		}
	}
}
