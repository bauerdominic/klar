FROM golang:1.15-buster as builder

ARG CLAIR_VERSION
ARG KLAR_EXECUTABLE_VERSION

COPY . /opt/gitlab
WORKDIR /opt/gitlab
RUN bash ./script/build pkg

FROM centos:centos8

ARG CLAIR_VERSION
ARG KLAR_EXECUTABLE_VERSION

ENV PATH="/opt/gitlab/bin:${PATH}"
ENV KLAR_EXECUTABLE_VERSION=${KLAR_EXECUTABLE_VERSION}
ENV SCANNER_VERSION=${CLAIR_VERSION}
ENV START_CLAIR_SERVER true

COPY --from=builder /opt/gitlab/pkg /opt/gitlab
RUN bash /opt/gitlab/script/install.sh
WORKDIR /
CMD ["analyzer", "run"]
