package reporter

import (
	"encoding/json"
	"fmt"
	"os"
	"path/filepath"

	"github.com/olekukonko/tablewriter"
	log "github.com/sirupsen/logrus"

	"gitlab.com/gitlab-org/security-products/analyzers/common/v2/command"
	"gitlab.com/gitlab-org/security-products/analyzers/common/v2/issue"
)

const (
	colorApproved   = "\033[1;36m%s\033[0m"
	colorUnapproved = "\033[1;31m%s\033[0m"
)

// ReportToFile writes the report to file, filtering out approved (allowlisted) vulnerabilites
// and remediations so that they don't show up in the file.  This function modifies both the
// Vulnerabilities and the Remediations fields of the given report argument.
func ReportToFile(report *issue.Report, unapproved map[string]bool, artifactDir string) error {
	// write indented JSON to artifact
	artifactPath := filepath.Join(artifactDir, command.ArtifactNameContainerScanning)
	f, err := os.OpenFile(artifactPath, os.O_CREATE|os.O_WRONLY|os.O_TRUNC, 0644)
	if err != nil {
		return err
	}
	defer f.Close()
	enc := json.NewEncoder(f)
	enc.SetIndent("", "  ")

	// don't output approved (allowlisted) vulnerabilities and their remediations
	filterApproved(report, unapproved)

	return enc.Encode(report)
}

// ReportToConsole outputs both approved and unapproved vulnerabilities in table format
func ReportToConsole(imageName string, report *issue.Report, unapproved map[string]bool) {
	if len(report.Vulnerabilities) > 0 {
		log.Warnf("Image [%s] contains %d total vulnerabilities", imageName, len(report.Vulnerabilities))

		if len(report.Remediations) > 0 {
			log.Warnf("Image [%s] contains %d remediations", imageName, len(report.Remediations))
		}

		if len(unapproved) > 0 {
			log.Errorf("Image [%s] contains %d unapproved vulnerabilities", imageName, len(unapproved))
		} else {
			log.Infof("Image [%s] contains NO unapproved vulnerabilities", imageName)
		}

		printTable(report.Vulnerabilities, unapproved)

	} else {
		log.Infof("Image [%s] contains NO unapproved vulnerabilities", imageName)
	}
}

// ExtIDForVulnerability takes as input an issue and returns the CVE value and a boolean
// representing whether it was possible to decode the CVE for the vulnerability
//
// Example:
//
// Given the following issue:
//
// {
//   "identifiers": [
//     {
//       "value": "RHSA-2015:1640",
//       "url": "https://access.redhat.com/errata/RHSA-2015:1640"
//     }
//   ]
// }
//
// Returns: RHSA-2015:1640
func ExtIDForVulnerability(vulnerability issue.Issue) string {
	if len(vulnerability.Identifiers) == 0 {
		return vulnerability.CompareKey
	}

	return vulnerability.Identifiers[0].Value
}

// compareKeyForRemediation returns the CompareKey from the remediation
func compareKeyForRemediation(remediation issue.Remediation) (string, bool) {
	if len(remediation.Fixes) != 1 {
		return "", false
	}

	return remediation.Fixes[0].CompareKey, true
}

func formatStatus(status string) string {
	if status == "Approved" {
		return fmt.Sprintf(colorApproved, status)
	}
	return fmt.Sprintf(colorUnapproved, status)
}

func formatTableData(vulnerabilities []issue.Issue, unapproved map[string]bool) [][]string {
	formatted := make([][]string, len(vulnerabilities))
	for i, vulnerability := range vulnerabilities {
		vulnerabilityCVE := ExtIDForVulnerability(vulnerability)

		status := "Approved"
		if unapproved[vulnerability.CompareKey] {
			status = "Unapproved"
		}

		vulnerabilityURL := ""
		if len(vulnerability.Links) > 0 {
			vulnerabilityURL = vulnerability.Links[0].URL
		}

		severity := vulnerability.Severity.String()
		severity = fmt.Sprintf("%s %s", severity, vulnerabilityCVE)

		formatted[i] = []string{
			formatStatus(status),
			severity,
			vulnerability.Location.Dependency.Package.Name,
			vulnerability.Location.Dependency.Version,
			vulnerability.Description + "\n\n" + vulnerabilityURL,
		}
	}
	return formatted
}

// printTable outputs a table version of the vulnerability report, marking approved (allowlisted)
// vulnerabilities with an "Approved" status, and other vulnerabilities as "Unapproved"
func printTable(vulnerabilities []issue.Issue, unapproved map[string]bool) {
	header := []string{"Status", "CVE Severity", "Package Name", "Package Version", "CVE Description"}
	table := tablewriter.NewWriter(os.Stdout)
	table.SetHeader(header)
	table.SetHeaderAlignment(tablewriter.ALIGN_LEFT)
	table.SetRowSeparator("-")
	table.SetRowLine(true)
	table.SetAlignment(tablewriter.ALIGN_LEFT)
	table.AppendBulk(formatTableData(vulnerabilities, unapproved))
	table.Render()
}

// filterApproved removes any allowlisted vulnerabilities from the list of Vulnerabilities
// and Remediations in the report
func filterApproved(report *issue.Report, unapproved map[string]bool) {
	unapprovedVulnerabilities := []issue.Issue{}
	unapprovedRemediations := []issue.Remediation{}

	for _, vulnerability := range report.Vulnerabilities {
		if key := vulnerability.CompareKey; unapproved[key] {
			unapprovedVulnerabilities = append(unapprovedVulnerabilities, vulnerability)
		}
	}

	for _, remediation := range report.Remediations {
		if key, ok := compareKeyForRemediation(remediation); ok && unapproved[key] {
			unapprovedRemediations = append(unapprovedRemediations, remediation)
		}
	}

	report.Vulnerabilities = unapprovedVulnerabilities
	report.Remediations = unapprovedRemediations
}
