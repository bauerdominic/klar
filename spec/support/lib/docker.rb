# frozen_string_literal: true

class Docker
  DEFAULT_ENV = {
    'CI_DEBUG_TRACE' => 'true',
    'CI_PROJECT_DIR' => '/tmp/app',
    'SECURE_LOG_LEVEL' => 'debug'
  }.freeze
  DEFAULT_IMAGE_NAME = 'klar:latest'.freeze

  attr_reader :pwd

  def initialize(pwd: Pathname.pwd)
    @pwd = pwd
  end

  def build(tag:)
    Dir.chdir pwd do
      system({ 'IMAGE_NAME' => DEFAULT_IMAGE_NAME }, './script/build', exception: true)
    end
  end

  def run(project:, env: {}, command:)
    env_options = DEFAULT_ENV.merge(env).map { |(key, value)| "--env #{key}='#{value}'" }
    Dir.chdir pwd do
      arguments = [
        :docker, :run, '--rm',
        "--entrypoint=/bin/sh",
        "--network=host",
        "--volume=#{project.path}:#{project.virtual_path}",
        "--workdir=#{project.virtual_path}",
        env_options
      ]
      if ENV['CI']
        ip = `getent hosts clair-vulnerabilities-db`.split(' ')[0]
        arguments.push("--add-host=clair-vulnerabilities-db:#{ip}")
      else
        arguments.push("--add-host=clair-vulnerabilities-db:127.0.0.1")
      end
      arguments.push(DEFAULT_IMAGE_NAME)
      arguments.push("-c '#{command}'") if command
      system(expand(arguments), exception: true)
    end
  end

  private

  def expand(command)
    command.flatten.map(&:to_s).join(' ')
  end
end
