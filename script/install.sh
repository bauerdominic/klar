#!/bin/sh

# This install script is used during
# the docker build process.

set -e

yum install -y ca-certificates git-core xz

# symlinks are here for backward compatibility with
# the old layout. /opt/gitlab is now the new home
# for all `container_scanning` artifacts.
mkdir -p /container-scanner
ln -s /opt/gitlab/bin/analyzer /analyzer
ln -s /opt/gitlab/bin/clair /clair
ln -s /opt/gitlab/bin/klar /klar
ln -s /opt/gitlab/config/clair-config.yaml.template /clair-config.yaml.template
