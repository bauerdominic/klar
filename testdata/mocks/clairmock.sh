#!/usr/bin/env sh

# this is a mock server used for testing the startup/shutdown of the Clair process

echo '{"Event":"running database migrations","Level":"info","Location":"pgsql.go:216","Time":"2019-12-02 03:57:56.660680"}'
echo '{"Event":"database migration ran successfully","Level":"info","Location":"pgsql.go:223","Time":"2019-12-02 03:57:56.691335"}'
echo '{"Event":"starting health API","Level":"info","Location":"api.go:85","Time":"2019-12-02 03:57:56.691598","port":6061}'
echo '{"Event":"notifier service is disabled","Level":"info","Location":"notifier.go:77","Time":"2019-12-02 03:57:56.692752"}'
echo '{"Event":"updater service is disabled.","Level":"info","Location":"updater.go:78","Time":"2019-12-02 03:57:56.692818"}'

# num_attempts is the number of times we loop before responding with a successful status
# set default to 0
num_attempts=${2:-0}
count=0
while true; do
    echo "${count}: $(date) clairmock running"
    >&2 echo "${count}: $(date) some error has occurred"
    # after num_attempts, start the API successfully
    if [ $count -eq $num_attempts ]; then
        echo '{"Event":"starting main API","Level":"info","Location":"api.go:52","Time":"2019-12-02 03:57:56.692977","port":6060}'
    fi
    sleep 1
    count=$((count+1))
done
