#!/usr/bin/env sh

# this is a mock server used for testing the report normally generated from the klar process

if [ "$#" -ne 1 ]; then
    echo "Illegal number of parameters"
    exit 1
fi

cat $(dirname "$0")/../klar-report-with-unresolvable-cve.json
